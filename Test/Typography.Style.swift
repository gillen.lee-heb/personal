//
//  Typography.Style.swift
//  Butter
//
//  Created by Lee Gillen on 3/23/21.
//

import Foundation

extension Typography {
    /// Typographic styles are the specific variants allowed based on the typography component
    ///
    /// - Note: These are defined from
    /// https://www.figma.com/file/GogHjd26L9Oi04YrvcyhItE4/Typography?node-id=867%3A259
    enum Style {
        case none
        case italic
        case underline
        case strikethrough
        case monospaceDigit
    }
}
