//
//  Typography.Weight.swift
//  Butter
//
//  Created by Lee Gillen on 3/19/21.
//

import UIKit

internal extension Typography {
    /// A list of the typography weights.
    ///
    /// - Note: These are defined from
    /// https://www.figma.com/file/GogHjd26L9Oi04YrvcyhItE4/Typography?node-id=867%3A259
    enum Weight {
        // Primary weights

        /// Primary weight XXL 900
        case xxl900
        /// Primary weight XL 900
        case xl900
        /// Primary weight LARGE 900
        case large900

        /// Primary weight BASE 400
        case base400
        /// Primary weight MEDIUM 400
        case medium400
        /// Primary weight SMALL 400
        case small400
        /// Primary weight XS 400
        case xs400

        // Secondary weights

        /// Secondary weight XXL 400
        case xxl400
        /// Secondary weight XL 400
        case xl400
        /// Secondary weight LARGE 400
        case large400

        /// Secondary weight BASE 800
        case base800
        /// Secondary weight MEDIUM 800
        case medium800
        /// Secondary weight SMALL 800
        case small800
        /// Secondary weight XS 800
        case xs800
    }
}

// Represents the specific values used for each of the base typographic types
internal extension Typography.Weight {
    /// New instance of a CTFont based on the current typography's font size
    var font: CTFont { UIFont.systemFont(ofSize: fontSize.value, weight: fontWeight.value) as CTFont }

    /// The supported line height based on the current typography's font size
    var lineHeight: CGFloat { fontSize.lineHeight }

    /// The supported minimum line height based on the current typography's font size
    var minimumLineHeight: CGFloat { fontSize.minimumLineHeight }

    /// The supported line spacing based on the current typography's font size
    var lineSpacing: CGFloat { fontSize.lineSpacing }

    /// The supported tracking (aka character spacing or kerning) based on the current typography's font size
    var tracking: CGFloat { fontSize.tracking }
}

extension Typography.Weight {
    /// All the typographic types that are allowed to be italicized
    ///
    /// - Note: These are defined from
    /// https://www.figma.com/file/GogHjd26L9Oi04YrvcyhItE4/Typography?node-id=867%3A259
    static let italicWeights = [
        Typography.Weight.base400,
        Typography.Weight.medium400,
        Typography.Weight.small400
    ]

    var supportsItalic: Bool {
        Self.italicWeights.contains(self)
    }

    /// All the typographic types that are allowed to be underlined
    ///
    /// - Note: These are defined from
    /// https://www.figma.com/file/GogHjd26L9Oi04YrvcyhItE4/Typography?node-id=867%3A259
    static let underlineWeights = [
        Typography.Weight.xs800
    ]

    var supportsUnderline: Bool {
        Self.underlineWeights.contains(self)
    }

    /// All the typographic types that are allowed to be strikethrough
    ///
    /// - Note: These are defined from
    /// https://www.figma.com/file/GogHjd26L9Oi04YrvcyhItE4/Typography?node-id=867%3A259
    static let strikethroughWeights = [
        Typography.Weight.medium800,
        Typography.Weight.medium400,
        Typography.Weight.small800,
        Typography.Weight.small400
    ]

    var supportsStrikethrough: Bool {
        Self.strikethroughWeights.contains(self)
    }

    /// All the typographic types that are allowed to be monospace digit
    ///
    /// - Note: These are defined from
    /// https://www.figma.com/file/GogHjd26L9Oi04YrvcyhItE4/Typography?node-id=867%3A259
    static let monospaceWeights = [
        Typography.Weight.base800,
        Typography.Weight.medium800,
        Typography.Weight.small800
    ]

    var supportsMonospaceDigits: Bool {
        Self.monospaceWeights.contains(self)
    }
}

extension Typography.Weight {
    /// The supported font sizes
    ///
    /// - Note: These are defined from
    /// https://www.figma.com/file/GogHjd26L9Oi04YrvcyhItE4/Typography?node-id=867%3A259
    enum FontSize: CGFloat {
        case xxl = 28
        case xl = 22
        case large = 20

        case base = 16
        case medium = 14
        case small = 12
        case xs = 10

        var value: CGFloat { rawValue }
    }

    /// Returns the font size based on the current typography weight
    var fontSize: Typography.Weight.FontSize {
        switch self {
        case .xxl900, .xxl400:
            return .xxl
        case .xl900, .xl400:
            return .xl
        case .large900, .large400:
            return .large
        case .base400, .base800:
            return .base
        case .medium400, .medium800:
            return .medium
        case .small400, .small800:
            return .small
        case .xs400, .xs800:
            return .xs
        }
    }
}

extension Typography.Weight.FontSize {
    /// The supported line heights
    ///
    /// - Note: These are defined from
    /// https://www.figma.com/file/GogHjd26L9Oi04YrvcyhItE4/Typography?node-id=867%3A259
    enum LineHeight: CGFloat {
        case xxl = 36
        case xl = 30
        case large = 26

        case base = 24
        case medium = 20
        case smallAndXS = 16

        var value: CGFloat { rawValue }
    }

    /// Returns the font size based on the current typography weight
    var lineHeight: CGFloat {
        switch self {
        case .xxl:
            return LineHeight.xxl.value
        case .xl:
            return LineHeight.xl.value
        case .large:
            return LineHeight.large.value
        case .base:
            return LineHeight.base.value
        case .medium:
            return LineHeight.medium.value
        case .small, .xs:
            return LineHeight.smallAndXS.value
        }
    }
}

extension Typography.Weight.FontSize {
    /// The supported minimum line heights
    ///
    /// - Note: These are defined from
    /// https://www.figma.com/file/GogHjd26L9Oi04YrvcyhItE4/Typography?node-id=867%3A259
    enum MinimumLineHeight: CGFloat {
        case xxl = 33
        case xl = 26
        case large = 24

        case base = 19
        case medium = 17
        case small = 14
        case xs = 12

        var value: CGFloat { rawValue }
    }

    /// Returns the minimum line height based on the current typography weight
    var minimumLineHeight: CGFloat {
        switch self {
        case .xxl:
            return MinimumLineHeight.xxl.value
        case .xl:
            return MinimumLineHeight.xl.value
        case .large:
            return MinimumLineHeight.large.value
        case .base:
            return MinimumLineHeight.base.value
        case .medium:
            return MinimumLineHeight.medium.value
        case .small:
            return MinimumLineHeight.small.value
        case .xs:
            return MinimumLineHeight.xs.value
        }
    }
}

extension Typography.Weight.FontSize {
    /// The supported tracking
    ///
    /// - Note: These are defined from
    /// https://www.figma.com/file/GogHjd26L9Oi04YrvcyhItE4/Typography?node-id=867%3A259
    enum Tracking: CGFloat {
        case xxl = -0.34
        case xl = -0.32
        case large = -0.43

        case base = 0.32
        case medium = 0.15
        case small = 0
        case xs = -0.11

        var value: CGFloat { rawValue }
    }

    /// Returns the tracking based on the current typography weight
    var tracking: CGFloat {
        switch self {
        case .xxl:
            return Tracking.xxl.value
        case .xl:
            return Tracking.xl.value
        case .large:
            return Tracking.large.value
        case .base:
            return Tracking.base.value
        case .medium:
            return Tracking.medium.value
        case .small:
            return Tracking.small.value
        case .xs:
            return Tracking.xs.value
        }
    }
}

extension Typography.Weight.FontSize {
    /// The supported line spacing
    ///
    /// - Note: These are defined from
    /// https://www.figma.com/file/GogHjd26L9Oi04YrvcyhItE4/Typography?node-id=867%3A259
    enum LineSpacing {
        case xxl
        case xl
        case large

        case base
        case medium
        case small
        case xs

        var value: CGFloat {
            switch self {
            case .xxl, .medium:
                return 3
            case .xl, .xs:
                return 4
            case .large, .small:
                return 2
            case .base:
                return 5
            }
        }
    }

    /// Returns the line spacing based on the current typography weight
    var lineSpacing: CGFloat {
        switch self {
        case .xxl:
            return LineSpacing.xxl.value
        case .xl:
            return LineSpacing.xl.value
        case .large:
            return LineSpacing.large.value
        case .base:
            return LineSpacing.base.value
        case .medium:
            return LineSpacing.medium.value
        case .small:
            return LineSpacing.small.value
        case .xs:
            return LineSpacing.xs.value
        }
    }
}

extension Typography.Weight {
    /// The supported font weight
    ///
    /// - Note: These are defined from
    /// https://www.figma.com/file/GogHjd26L9Oi04YrvcyhItE4/Typography?node-id=867%3A259
    enum FontWeight {
        case black900
        case regular400
        case heavy800

        /// The value for these weights are standardized using UIFont.Weight.
        ///
        /// - Note: `UIFont` is used here because it can be toll-free bridged to and from `CTFont`. `SwiftUI.Font` can also be initialized from `CTFont`.
        var value: UIFont.Weight {
            switch self {
            case .black900:
                return .black // swiftlint:disable:this no_system_uicolor
            case .regular400:
                return .regular
            case .heavy800:
                return .heavy
            }
        }
    }

    /// Returns the font weight based on the current typography weight
    var fontWeight: FontWeight {
        switch self {
        case .xxl900, .xl900, .large900:
            return .black900
        case .xxl400, .xl400, .large400, .base400, .medium400, .small400, .xs400:
            return .regular400
        case .base800, .medium800, .small800, .xs800:
            return .heavy800
        }
    }
}
