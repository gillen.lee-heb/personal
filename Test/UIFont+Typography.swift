//
//  UIFont+Typography.swift
//  Butter
//
//  Created by Lee Gillen on 3/23/21.
//

import UIKit

extension UIFont {
    /// Returns a new font with the italics trait.
    func italics() -> UIFont {
        let descriptor = fontDescriptor
        let traits = descriptor.symbolicTraits.union(.traitItalic)
        guard let italicsDescriptor = descriptor.withSymbolicTraits(traits) else {
            // failed to apply italics
            return self
        }

        // size 0 means keep the size as it is
        return UIFont(descriptor: italicsDescriptor, size: 0)
    }

    /// Returns a new font with the italics trait.
    func monospaceDigit() -> UIFont {
        let fontDescriptorFeatureSettings = [[
            UIFontDescriptor.FeatureKey.featureIdentifier: kNumberSpacingType,
            UIFontDescriptor.FeatureKey.typeIdentifier: kMonospacedNumbersSelector
        ]]
        let fontDescriptorAttributes = [
            UIFontDescriptor.AttributeName.featureSettings: fontDescriptorFeatureSettings
        ]

        let descriptor = fontDescriptor.addingAttributes(fontDescriptorAttributes)

        // size 0 means keep the size as it is
        return UIFont(descriptor: descriptor, size: 0)
    }
}

extension CTFont {
    /// Returns a new font with the italics trait.
    func italics() -> CTFont {
        (self as UIFont).italics()
    }

    /// Returns a new font with the italics trait.
    func monospaceDigit() -> CTFont {
        (self as UIFont).monospaceDigit()
    }
}
