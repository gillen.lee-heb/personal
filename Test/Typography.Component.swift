//
//  Typography.Component.swift
//  Butter
//
//  Created by Lee Gillen on 3/19/21.
//

import CoreGraphics
import CoreText
import Foundation

extension Typography {
    /// A list of the typography components
    ///
    /// - Note: These are defined from
    /// https://www.figma.com/file/GogHjd26L9Oi04YrvcyhItE4/Typography?node-id=867%3A259
    enum Component {
        case title1
        case title2
        case title3
        case title4
        case title5
        case title6
        case body1
        case caption1
        case subcaption1
        case footnote1
        case body2
        case caption2
        case subcaption2
        case footnote2
    }
}

// Represents the specific values used for each of the components
extension Typography.Component {
    /// Returns a new instance of a CTFont based on the current typography's font size
    var font: CTFont { weight.font }

    /// The supported line height based on the current typography's font size
    var lineHeight: CGFloat { weight.lineHeight }

    /// The supported minimum line height based on the current typography's font size
    var minimumLineHeight: CGFloat { weight.minimumLineHeight }

    /// The supported line spacing based on the current typography's font size
    var lineSpacing: CGFloat { weight.lineSpacing }

    /// The supported tracking (aka character spacing or kerning) based on the current typography's font size
    var tracking: CGFloat { weight.tracking }

    /// Does the typography component support italic
    var supportsItalic: Bool { weight.supportsItalic }

    /// Does the typography component support underline
    var supportsUnderline: Bool { weight.supportsUnderline }

    /// Does the typography component support strikethrough
    var supportsStrikethrough: Bool { weight.supportsStrikethrough }

    /// Does the typography component support monospace digits
    var supportsMonospaceDigits: Bool { weight.supportsMonospaceDigits }
}

private extension Typography.Component {
    /// The associated weight of the typography component.
    ///
    /// - Note: These are defined from
    /// https://www.figma.com/file/GogHjd26L9Oi04YrvcyhItE4/Typography?node-id=867%3A259
    var weight: Typography.Weight {
        switch self {
        case .title1:
            return .xxl900
        case .title2:
            return .xxl400
        case .title3:
            return .xl900
        case .title4:
            return .xl400
        case .title5:
            return .large900
        case .title6:
            return .large400
        case .body1:
            return .base400
        case .caption1:
            return .medium400
        case .subcaption1:
            return .small400
        case .footnote1:
            return .xs400
        case .body2:
            return .base800
        case .caption2:
            return .medium800
        case .subcaption2:
            return .small800
        case .footnote2:
            return .xs800
        }
    }
}
