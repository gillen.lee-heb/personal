//
//  Typography.swift
//  Butter
//
//  Created by Lee Gillen on 3/19/21.
//

import CoreGraphics
import CoreText
import Foundation

/// A namespace for types that define the values used in the DLS typography.
///
/// Each typography case is used to specify the font, line spacing, character spacing, and style being applied.
/// - Note: These values are defined from
/// https://www.figma.com/file/GogHjd26L9Oi04YrvcyhItE4/Typography?node-id=867%3A259
public enum Typography {
    case title1
    case title2
    case title3
    case title4
    case title5
    case title6

    case body1
    case body1Italic

    case body2
    case body2MonospacedDigit

    case caption1
    case caption1Italic
    case caption1Strikethrough

    case caption2
    case caption2Strikethrough
    case caption2MonospacedDigit

    case subcaption1
    case subcaption1Italic
    case subcaption1Strikethrough

    case subcaption2
    case subcaption2Strikethrough
    case subcaption2MonospacedDigit

    case footnote1

    case footnote2
    case footnote2Underline
}

// Represents the specific values used
public extension Typography {
    /// Returns a new instance of a CTFont based on the current typography and it's style
    var font: CTFont {
        let font = component.font
        if supportsItalic { return font.italics() }
        if supportsMonospacedDigit { return font.monospaceDigit() }
        return font
    }

    /// The supported line height based on the current typography's font size
    var lineHeight: CGFloat { component.lineHeight }

    /// The supported minimum line height based on the current typography's font size
    var minimumLineHeight: CGFloat { component.minimumLineHeight }

    /// The supported line spacing based on the current typography's font size
    var lineSpacing: CGFloat { component.lineSpacing }

    /// The supported tracking (aka character spacing or kerning) based on the current typography's font size
    var tracking: CGFloat { component.tracking }

    /// Does the type support underline
    var supportsUnderline: Bool { component.supportsUnderline && styles.contains(.underline) }

    /// Does the type support strikethrough
    var supportsStrikethrough: Bool { component.supportsStrikethrough && styles.contains(.strikethrough) }

    /// Does the type support strikethrough
    var supportsItalic: Bool { component.supportsItalic && styles.contains(.italic) }

    /// Does the type support strikethrough
    var supportsMonospacedDigit: Bool { component.supportsMonospaceDigits && styles.contains(.monospaceDigit) }
}

// This extension breaks down the current typography to the Typography.Component and Typography.Style
extension Typography {
    var component: Typography.Component {
        switch self {
        case .title1:
            return .title1
        case .title2:
            return .title2
        case .title3:
            return .title3
        case .title4:
            return .title4
        case .title5:
            return .title5
        case .title6:
            return .title6
        case .body1:
            return .body1
        case .body1Italic:
            return .body1
        case .body2, .body2MonospacedDigit:
            return .body2
        case .caption1, .caption1Italic, .caption1Strikethrough:
            return .caption1
        case .caption2, .caption2Strikethrough, .caption2MonospacedDigit:
            return .caption2
        case .subcaption1, .subcaption1Italic, .subcaption1Strikethrough:
            return .subcaption1
        case .subcaption2, .subcaption2Strikethrough, .subcaption2MonospacedDigit:
            return .subcaption2
        case .footnote1:
            return .footnote1
        case .footnote2, .footnote2Underline:
            return .footnote2
        }
    }

    var styles: [Typography.Style] {
        switch self {
        case .title1, .title2, .title3, .title4, .title5, .title6, .body1, .body2, .caption1, .caption2, .subcaption1, .subcaption2, .footnote1, .footnote2:
            return [.none]

        case .caption1Strikethrough, .caption2Strikethrough, .subcaption1Strikethrough, .subcaption2Strikethrough:
            return [.strikethrough]

        case .footnote2Underline:
            return [.underline]

        case .body2MonospacedDigit, .caption2MonospacedDigit, .subcaption2MonospacedDigit:
            return [.monospaceDigit]

        case .body1Italic, .caption1Italic, .subcaption1Italic:
            return [.italic]
        }
    }
}
