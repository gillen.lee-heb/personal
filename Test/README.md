#  Typography

DLS typography is a complex set of values that define not just the font and styling of the text, but the character and line spacing that are critical to implementing designs that are using DLS components.

## Table of Contents
1. [Typography](#Typography)
2. [Usage](#Usage)
3. [Conversion from Zeplin Fonts](#Conversion)
4. [SwiftUI](#SwiftUI)

## Typography <a name="Typography"></a>

[`enum Typography`](Typography.swift) contains the semantic types of typography. These not only indicate the typographic component being used, but the style being applied as well.

For example, `body1` is a typographic component. When the italic style is applied the typography is `body1Italic`.

| Case                          | Preview                                                                       |
| ----                          | -------                                                                       |
| `title1`                      | ![alt text](demo/title1.png "title1")                                         |
| `title2`                      | ![alt text](demo/title2.png "title2")                                         |
| `title3`                      | ![alt text](demo/title3.png "title3")                                         |
| `title4`                      | ![alt text](demo/title4.png "title4")                                         |
| `title5`                      | ![alt text](demo/title5.png "title5")                                         |
| `title6`                      | ![alt text](demo/title6.png "title6")                                         |
| `body1`                       | ![alt text](demo/body1.png "body1")                                           |
| `body1Italic`                 | ![alt text](demo/body1Italic.png "body1Italic")                               |
| `body2`                       | ![alt text](demo/body2.png "body2")                                           |
| `body2MonospacedDigit`        | ![alt text](demo/body2MonospacedDigit.png "body2MonospacedDigit")             |
| `caption1`                    | ![alt text](demo/caption1.png "caption1")                                     |
| `caption1Italic`              | ![alt text](demo/caption1Italic.png "caption1Italic")                         |
| `caption1Strikethrough`       | ![alt text](demo/caption1Strikethrough.png "caption1Strikethrough")           |
| `caption2`                    | ![alt text](demo/caption2.png "caption2")                                     |
| `caption2Strikethrough`       | ![alt text](demo/caption2Strikethrough.png "caption2Strikethrough")           |
| `caption2MonospacedDigit`     | ![alt text](demo/caption2MonospacedDigit.png "caption2MonospacedDigit")       |
| `subcaption1`                 | ![alt text](demo/subcaption1.png "subcaption1")                               |
| `subcaption1Italic`           | ![alt text](demo/subcaption1Italic.png "subcaption1Italic")                   |
| `subcaption1Strikethrough`    | ![alt text](demo/subcaption1Strikethrough.png "subcaption1Strikethrough")     |
| `subcaption2`                 | ![alt text](demo/subcaption2.png "subcaption2")                               |
| `subcaption2Strikethrough`    | ![alt text](demo/subcaption2Strikethrough.png "subcaption2Strikethrough")     |
| `subcaption2MonospacedDigit`  | ![alt text](demo/subcaption2MonospacedDigit.png "subcaption2MonospacedDigit") |
| `footnote1`                   | ![alt text](demo/footnote1.png "footnote1")                                   |
| `footnote2`                   | ![alt text](demo/footnote2.png "footnote2")                                   |
| `footnote2Underline`          | ![alt text](demo/footnote2Underline.png "footnote2Underline")                 |


Each of the typographies above describes not only the font, size and weight. It also describes specific spacing values including line height, line spacing and tracking. It also includes any specific styling such as italics, underline, strikethrough, or monospace digits. 

> Note: Not every typography case supports any or all styles.

## Usage <a name="Usage"></a>

Your designs in Figma should indicate typography to use. For example if the text is defined as `Title 1` then in your `UILabel` instance you would use the following:

```
@IBOutlet var titleLabel: UILabel! {
    didSet {
        titleLabel
            // or .title1() if you already have a title set in IB
            .title1("My awesome title")
    }
}
```

As the example above demonstrates, the label is applying the `title1` typography with the given string. Each of the typography types has a corresponding `UILabel` function to setup the labels attributed string. These functions are the recommended way to functionally apply the appropriate typography to the label. You may omit the text in the function call if your label has already set the text property elsewhere.

| Typography                    | Matching UILabel function                 |
| ----------                    | -------------------------                 |
| `title1`                      | `yourLabel.title1()`                      |
| `title2`                      | `yourLabel.title2()`                      |
| `title3`                      | `yourLabel.title3()`                      |
| `title4`                      | `yourLabel.title4()`                      |
| `title5`                      | `yourLabel.title5()`                      |
| `title6`                      | `yourLabel.title6()`                      |
| `body1`                       | `yourLabel.body1()`                       |
| `body1Italic`                 | `yourLabel.body1Italic()`                 |
| `body2`                       | `yourLabel.body2()`                       |
| `body2MonospacedDigit`        | `yourLabel.body2MonospacedDigit() `       |
| `caption1`                    | `yourLabel.caption1()`                    |
| `caption1Italic`              | `yourLabel.caption1Italic()`              |
| `caption1Strikethrough`       | `yourLabel.caption1Strikethrough()`       |
| `caption2`                    | `yourLabel.caption2()`                    |
| `caption2Strikethrough`       | `yourLabel.caption2Strikethrough()`       |
| `caption2MonospacedDigit`     | `yourLabel.caption2MonospacedDigit()`     |
| `subcaption1`                 | `yourLabel.subcaption1()`                 |
| `subcaption1Italic`           | `yourLabel.subcaption1Italic()`           |
| `subcaption1Strikethrough`    | `yourLabel.subcaption1Strikethrough()`    |
| `subcaption2`                 | `yourLabel.subcaption2()`                 |
| `subcaption2Strikethrough`    | `yourLabel.subcaption2Strikethrough()`    |
| `subcaption2MonospacedDigit`  | `yourLabel.subcaption2MonospacedDigit()`  |
| `footnote1`                   | `yourLabel.footnote1()`                   |
| `footnote2`                   | `yourLabel.footnote2()`                   |
| `footnote2Underline`          | `yourLabel.footnote2Underline()`          |

## Conversion from Zeplin Fonts to Typography <a name="Conversion"></a>

If you are looking at an old design that has not yet migrated to the DLS typography. Then use the following table as a guide.

The Zeplin cases listed below are from the [UIFont+Additions.swift](UIFont+Additions.swift). These cases will be removed when we are no longer referencing them or have migrated off of them.

| Zeplin Case               | Relation to   | Typography case                           | Use short term                                             |
| -----------               | -----------   | ---------------                           | --------------                                             |
| `largeTitle`              | Approximately | `Typography.title1`                       | `yourLabel.font = Typography.largeTitle.font`              |
| `title0`                  | Exactly       | `Typography.title1`                       | `yourLabel.font = Typography.title0.font`                  |
| `title1`                  | Approximately | `Typography.title1`                       | `yourLabel.font = Typography.title1.font`                  |
| `title3`                  | Approximately | `Typography.title3`                       | `yourLabel.font = Typography.title3.font`                  |
| `title5`                  | Exactly       | `Typography.title3`                       | `yourLabel.font = Typography.title5.font`                  |
| `title6`                  | Exactly       | `Typography.title5`                       | `yourLabel.font = Typography.title6.font`                  |
| `headline0`               | Approximately | `Typography.title5`                       | `yourLabel.font = Typography.headline0.font`               |
| `headline1`               | Approximately | `Typography.body2`                        | `yourLabel.font = Typography.headline1.font`               |
| `headline2`               | Approximately | `Typography.title6`                       | `yourLabel.font = Typography.headline2.font`               |
| `body1`                   | Exactly       | `Typography.body2`                        | `yourLabel.font = Typography.body1.font`                   |
| `body1MonospacedDigit`    | Exactly       | `Typography.body2MonospacedDigit`         | `yourLabel.font = Typography.body1MonospacedDigit.font`    |
| `body2`                   | Exactly       | `Typography.body1`                        | `yourLabel.font = Typography.body2.font`                   |
| `body2Italic`             | Exactly       | `Typography.body1Italic`                  | `yourLabel.font = Typography.body2Italic.font`             |
| `body3MonospacedDigit`    | Approximately | `Typography.body2MonospacedDigit`         | `yourLabel.font = Typography.body3MonospacedDigit.font`    |
| `subhead1`                | Exactly       | `Typography.caption2`                     | `yourLabel.font = Typography.subhead1.font`                |
| `subhead1MonospacedDigit` | Exactly       | `Typography.caption2MonospacedDigit`      | `yourLabel.font = Typography.subhead1MonospacedDigit.font` |
| `subhead2`                | Exactly       | `Typography.caption1`                     | `yourLabel.font = Typography.subhead2.font`                |
| `subhead2SemiBold`        | Approximately | `Typography.caption2`                     | `yourLabel.font = Typography.subhead2SemiBold.font`        |
| `subhead2Italic`          | Exactly       | `Typography.caption1Italic`               | `yourLabel.font = Typography.subhead2Italic.font`          |
| `caption1`                | Exactly       | `Typography.subcaption2`                  | `yourLabel.font = Typography.caption1.font`                |
| `caption1MonospacedDigit` | Exactly       | `Typography.subcaption2MonospacedDigit`   | `yourLabel.font = Typography.caption1MonospacedDigit.font` |
| `caption2`                | Exactly       | `Typography.subcaption1`                  | `yourLabel.font = Typography.caption2.font`                |
| `caption2Italic`          | Exactly       | `Typography.subcaption1Italic`            | `yourLabel.font = Typography.caption2Italic.font`          |
| `caption2SemiBold`        | Approximately | `Typography.subcaption2`                  | `yourLabel.font = Typography.caption2SemiBold.font`        |
| `caption3`                | Exactly       | `Typography.footnote2`                    | `yourLabel.font = Typography.caption3.font`                |
| `caption4`                | Exactly       | `Typography.footnote1`                    | `yourLabel.font = Typography.caption4.font`                |

> Note: We will not be supporting this direct use of the typography case's font long-term. We prefer that the text being displayed be updated to use the [functional usage](#Usage) defined above.

## SwiftUI <a name="SwiftUI"></a>

We plan to add the text builder implementation as documented in the [RFC for typography](https://hebecom.atlassian.net/wiki/spaces/MYHEBIOS/pages/1821609160/RFC-iOS-0044+-+Implementing+complex+typography?focusedCommentId=1866367222#comment-1866367222) soon. This builder will be used to implement the [existing UILabel calls](#Usage) as mentioned above and also allow for the same for `SwiftUI.Text`.