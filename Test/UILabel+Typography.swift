//
//  UILabel+Typography.swift
//  Butter
//
//  Created by Lee Gillen on 3/22/21.
//

import UIKit

extension UILabel {
    /// Applies `Typography.title1` using the existing text
    /// Note: This sets the `attributedText` property using the string provided or the existing text.
    /// If you provide neither, you will need to update the `attributedText` manually.
    func title1(_ newText: String? = nil) {
        attributedText = Typography.title1.asAttributedString(newText ?? text ?? "")
    }

    /// Applies `Typography.title2` using the existing text
    /// Note: This sets the `attributedText` property using the string provided or the existing text.
    /// If you provide neither, you will need to update the `attributedText` manually.
    func title2(_ newText: String? = nil) {
        attributedText = Typography.title2.asAttributedString(newText ?? text ?? "")
    }

    /// Applies `Typography.title3` using the existing text
    /// Note: This sets the `attributedText` property using the string provided or the existing text.
    /// If you provide neither, you will need to update the `attributedText` manually.
    func title3(_ newText: String? = nil) {
        attributedText = Typography.title3.asAttributedString(newText ?? text ?? "")
    }

    /// Applies `Typography.title4` using the existing text
    /// Note: This sets the `attributedText` property using the string provided or the existing text.
    /// If you provide neither, you will need to update the `attributedText` manually.
    func title4(_ newText: String? = nil) {
        attributedText = Typography.title4.asAttributedString(newText ?? text ?? "")
    }

    /// Applies `Typography.title5` using the existing text
    /// Note: This sets the `attributedText` property using the string provided or the existing text.
    /// If you provide neither, you will need to update the `attributedText` manually.
    func title5(_ newText: String? = nil) {
        attributedText = Typography.title5.asAttributedString(newText ?? text ?? "")
    }

    /// Applies `Typography.title6` using the existing text
    /// Note: This sets the `attributedText` property using the string provided or the existing text.
    /// If you provide neither, you will need to update the `attributedText` manually.
    func title6(_ newText: String? = nil) {
        attributedText = Typography.title6.asAttributedString(newText ?? text ?? "")
    }

    /// Applies `Typography.body1` using the existing text
    /// Note: This sets the `attributedText` property using the string provided or the existing text.
    /// If you provide neither, you will need to update the `attributedText` manually.
    func body1(_ newText: String? = nil) {
        attributedText = Typography.body1.asAttributedString(newText ?? text ?? "")
    }

    /// Applies `Typography.body1Italic` using the existing text
    /// Note: This sets the `attributedText` property using the string provided or the existing text.
    /// If you provide neither, you will need to update the `attributedText` manually.
    func body1Italic(_ newText: String? = nil) {
        attributedText = Typography.body1Italic.asAttributedString(newText ?? text ?? "")
    }

    /// Applies `Typography.body2` using the existing text
    /// Note: This sets the `attributedText` property using the string provided or the existing text.
    /// If you provide neither, you will need to update the `attributedText` manually.
    func body2(_ newText: String? = nil) {
        attributedText = Typography.body2.asAttributedString(newText ?? text ?? "")
    }

    /// Applies `Typography.body2MonospacedDigit` using the existing text
    /// Note: This sets the `attributedText` property using the string provided or the existing text.
    /// If you provide neither, you will need to update the `attributedText` manually.
    func body2MonospacedDigit(_ newText: String? = nil) {
        attributedText = Typography.body2MonospacedDigit.asAttributedString(newText ?? text ?? "")
    }

    /// Applies `Typography.caption1` using the existing text
    /// Note: This sets the `attributedText` property using the string provided or the existing text.
    /// If you provide neither, you will need to update the `attributedText` manually.
    func caption1(_ newText: String? = nil) {
        attributedText = Typography.caption1.asAttributedString(newText ?? text ?? "")
    }

    /// Applies `Typography.caption1Italic` using the existing text
    /// Note: This sets the `attributedText` property using the string provided or the existing text.
    /// If you provide neither, you will need to update the `attributedText` manually.
    func caption1Italic(_ newText: String? = nil) {
        attributedText = Typography.caption1Italic.asAttributedString(newText ?? text ?? "")
    }

    /// Applies `Typography.caption1Strikethrough` using the existing text
    /// Note: This sets the `attributedText` property using the string provided or the existing text.
    /// If you provide neither, you will need to update the `attributedText` manually.
    func caption1Strikethrough(_ newText: String? = nil) {
        attributedText = Typography.caption1Strikethrough.asAttributedString(newText ?? text ?? "")
    }

    /// Applies `Typography.caption2` using the existing text
    /// Note: This sets the `attributedText` property using the string provided or the existing text.
    /// If you provide neither, you will need to update the `attributedText` manually.
    func caption2(_ newText: String? = nil) {
        attributedText = Typography.caption2.asAttributedString(newText ?? text ?? "")
    }

    /// Applies `Typography.caption2Strikethrough` using the existing text
    /// Note: This sets the `attributedText` property using the string provided or the existing text.
    /// If you provide neither, you will need to update the `attributedText` manually.
    func caption2Strikethrough(_ newText: String? = nil) {
        attributedText = Typography.caption2Strikethrough.asAttributedString(newText ?? text ?? "")
    }

    /// Applies `Typography.caption2MonospacedDigit` using the existing text
    /// Note: This sets the `attributedText` property using the string provided or the existing text.
    /// If you provide neither, you will need to update the `attributedText` manually.
    func caption2MonospacedDigit(_ newText: String? = nil) {
        attributedText = Typography.caption2MonospacedDigit.asAttributedString(newText ?? text ?? "")
    }

    /// Applies `Typography.subcaption1` using the existing text
    /// Note: This sets the `attributedText` property using the string provided or the existing text.
    /// If you provide neither, you will need to update the `attributedText` manually.
    func subcaption1(_ newText: String? = nil) {
        attributedText = Typography.subcaption1.asAttributedString(newText ?? text ?? "")
    }

    /// Applies `Typography.subcaption1Italic` using the existing text
    /// Note: This sets the `attributedText` property using the string provided or the existing text.
    /// If you provide neither, you will need to update the `attributedText` manually.
    func subcaption1Italic(_ newText: String? = nil) {
        attributedText = Typography.subcaption1Italic.asAttributedString(newText ?? text ?? "")
    }

    /// Applies `Typography.subcaption1Strikethrough` using the existing text
    /// Note: This sets the `attributedText` property using the string provided or the existing text.
    /// If you provide neither, you will need to update the `attributedText` manually.
    func subcaption1Strikethrough(_ newText: String? = nil) {
        attributedText = Typography.subcaption1Strikethrough.asAttributedString(newText ?? text ?? "")
    }

    /// Applies `Typography.subcaption2` using the existing text
    /// Note: This sets the `attributedText` property using the string provided or the existing text.
    /// If you provide neither, you will need to update the `attributedText` manually.
    func subcaption2(_ newText: String? = nil) {
        attributedText = Typography.subcaption2.asAttributedString(newText ?? text ?? "")
    }

    /// Applies `Typography.subcaption2Strikethrough` using the existing text
    /// Note: This sets the `attributedText` property using the string provided or the existing text.
    /// If you provide neither, you will need to update the `attributedText` manually.
    func subcaption2Strikethrough(_ newText: String? = nil) {
        attributedText = Typography.subcaption2Strikethrough.asAttributedString(newText ?? text ?? "")
    }

    /// Applies `Typography.subcaption2MonospacedDigit` using the existing text
    /// Note: This sets the `attributedText` property using the string provided or the existing text.
    /// If you provide neither, you will need to update the `attributedText` manually.
    func subcaption2MonospacedDigit(_ newText: String? = nil) {
        attributedText = Typography.subcaption2MonospacedDigit.asAttributedString(newText ?? text ?? "")
    }

    /// Applies `Typography.footnote1` using the existing text
    /// Note: This sets the `attributedText` property using the string provided or the existing text.
    /// If you provide neither, you will need to update the `attributedText` manually.
    func footnote1(_ newText: String? = nil) {
        attributedText = Typography.footnote1.asAttributedString(newText ?? text ?? "")
    }

    /// Applies `Typography.footnote2` using the existing text
    /// Note: This sets the `attributedText` property using the string provided or the existing text.
    /// If you provide neither, you will need to update the `attributedText` manually.
    func footnote2(_ newText: String? = nil) {
        attributedText = Typography.footnote2.asAttributedString(newText ?? text ?? "")
    }

    /// Applies `Typography.footnote2Underline` using the existing text
    /// Note: This sets the `attributedText` property using the string provided or the existing text.
    /// If you provide neither, you will need to update the `attributedText` manually.
    func footnote2Underline(_ newText: String? = nil) {
        attributedText = Typography.footnote2Underline.asAttributedString(newText ?? text ?? "")
    }
}

extension Typography {
    func asAttributedString(_ string: String) -> NSAttributedString {
        let paragraph = NSMutableParagraphStyle()
        paragraph.minimumLineHeight = minimumLineHeight
        paragraph.lineSpacing = lineSpacing

        let trackingKey: NSAttributedString.Key = {
            guard #available(iOS 14, *) else { return .kern }
            return .tracking
        }()

        var attributes: [NSAttributedString.Key: Any]
        attributes = [.font: font,
                      .paragraphStyle: paragraph,
                      trackingKey: tracking]

        if supportsUnderline {
            attributes[.underlineStyle] = NSUnderlineStyle.single.rawValue
        }
        if supportsStrikethrough {
            attributes[.strikethroughStyle] = NSUnderlineStyle.single.rawValue
        }

        return NSAttributedString(string: string,
                                  attributes: attributes)
    }
}
