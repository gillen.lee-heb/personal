/// A playground for demonstrating a text builder
import PlaygroundSupport
import SwiftUI
import UIKit

// We use CTFont throughout the builder since a CTFont is able to toll-free bridge between UIFont and you can initialize SwiftUI.Font(ctFont).
let ctFont = UIFont.systemFont(ofSize: 24, weight: .black) as CTFont

// Build a generic builder that uses all known decorators
let genericBuilder = TextBuilder()
    .string("ABCD 0123")
    .font(ctFont)
    .italic() // skipped because it is overridden by monospace() below
    .paragraph(minimumLineHeight: 20, lineSpacing: 20)
    .strikethrough()
    .underline()
    .tracking(20)
    .monospace()

// Build a XXL 900 text with strikethrough style ignored
let builder = WeightTextBuilder(weight: .xxl900)
    .string("Howdy")
    .strikethrough() // does not apply

// Build a SMALL 400 text with strikethrough and italic style
let small400Builder = WeightTextBuilder(weight: .small400)
    .string("Howdy")
    .strikethrough() // applies
    .italic() // does not apply

// Build a SMALL 400 text with strikethrough and italic style
//let subCaption1Builder = ComponentTextBuilder(component: .subcaption1)
//    .string("Howdy")
//    .strikethrough() // applies
//    .italic() // does not apply

@IBOutlet var titleLabel: UILabel {
    didSet {
//        let titleBuilder = ComponentTextBuilder(component: .title1)
        titleLabel.title1()
    }
}

class ComponentLabel: UILabel {
    let titleBuilder: ComponentTextBuilder
    override init(frame: CGRect) {
        let titleBuilder = ComponentTextBuilder
            .title1()
            .string(titleLabel.text)
        titleBuilder.updatingLabel(titleLabel)
    }

    func strikethrough() {}
}

class Title1Label: ComponentLabel {
    override func strikethrough() {}
}

PlaygroundPage
    .current
    .liveView = DemoController()
    .adding(label: genericBuilder.buildLabel(), text: genericBuilder.buildText())
