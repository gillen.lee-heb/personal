import Foundation

/// A namespace for types that define the values use in DLS typography.
///
/// The various types defined as extensions on ``Typography`` implement their functionality as additional enumerations that extend this enumeration. For example, the `Component` enumeration has a property  `weight` that returns a `Typography.Weight` case.
/// - Note: These enumerations and their values are defined from
/// https://www.figma.com/file/GogHjd26L9Oi04YrvcyhItE4/Typography?node-id=867%3A259
 public enum Typography {}

public extension Typography {
    /// A list of the typography components
    ///
    /// - Note: These are defined from
    /// https://www.figma.com/file/GogHjd26L9Oi04YrvcyhItE4/Typography?node-id=867%3A259
    enum Component {
        case title1
        case title2
        case title3
        case title4
        case title5
        case title6
        case body1
        case caption1
        case subcaption1
        case footnote1
        case body2
        case caption2
        case subcaption2
        case footnote2
    }
}

public extension Typography.Component {
    /// The associated weight of the typography component.
    ///
    /// - Note: These are defined from
    /// https://www.figma.com/file/GogHjd26L9Oi04YrvcyhItE4/Typography?node-id=867%3A259
    var weight: Typography.Weight {
        switch self {
        case .title1:
            return .xxl900
        case .title2:
            return .xxl400
        case .title3:
            return .xl900
        case .title4:
            return .xl400
        case .title5:
            return .large900
        case .title6:
            return .large400
        case .body1:
            return .base400
        case .caption1:
            return .medium400
        case .subcaption1:
            return .small400
        case .footnote1:
            return .xs400
        case .body2:
            return .base800
        case .caption2:
            return .medium800
        case .subcaption2:
            return .small800
        case .footnote2:
            return .xs800
        }
    }
}

extension Typography.Component {
    /// Typographic styles are the specific variants allowed based on the base typographic type
    ///
    /// - Note: These are defined from
    /// https://www.figma.com/file/GogHjd26L9Oi04YrvcyhItE4/Typography?node-id=867%3A259
    enum Style {
        case none
        case italic
        case underline
        case strikethrough
        case monospace
    }

    /// Validates the provided style with the current typography weight to see if it is supported. If it is supported the style is returned, otherwise .none is returned.
    func validate(_ style: Style) -> Style {
        switch style {
        case .italic:
            return italic
        case .monospace:
            return monospace
        case .strikethrough:
            return strikethrough
        case .underline:
            return underline
        case .none:
            return .none
        }
    }

    /// All the typographic types that are allowed to be italicized
    ///
    /// - Note: These are defined from
    /// https://www.figma.com/file/GogHjd26L9Oi04YrvcyhItE4/Typography?node-id=867%3A259
    static let italic = [
        .body1,
        .caption1,
        .subcaption1
    ]

    var isItalic: Bool {
        Self.italic.contains(self)
    }

    /// Applies the italic style if allowed, otherwise returns none
    var italic: Style {
        guard isItalic else { return .none }
        return .italic
    }

    /// All the typographic types that are allowed to be underlined
    ///
    /// - Note: These are defined from
    /// https://www.figma.com/file/GogHjd26L9Oi04YrvcyhItE4/Typography?node-id=867%3A259
    static let underline = [
        Typography.Weight.xs800
    ]

    var isUnderline: Bool {
        Self.underline.contains(self)
    }

    /// Applies the underline style if allowed, otherwise returns none
    var underline: Style {
        guard isUnderline else { return .none }
        return .underline
    }

    /// All the typographic types that are allowed to be strikethrough
    ///
    /// - Note: These are defined from
    /// https://www.figma.com/file/GogHjd26L9Oi04YrvcyhItE4/Typography?node-id=867%3A259
    static let strikethrough = [
        Typography.Weight.medium800,
        Typography.Weight.medium400,
        Typography.Weight.small800,
        Typography.Weight.small400
    ]

    var isStrikethrough: Bool {
        Self.strikethrough.contains(self)
    }

    /// Applies the strikethrough style if allowed, otherwise returns none
    var strikethrough: Style {
        guard isStrikethrough else { return .none }
        return .strikethrough
    }

    /// All the typographic types that are allowed to be monospace digit
    ///
    /// - Note: These are defined from
    /// https://www.figma.com/file/GogHjd26L9Oi04YrvcyhItE4/Typography?node-id=867%3A259
    static let monospace = [
        Typography.Weight.base800,
        Typography.Weight.medium800,
        Typography.Weight.small800
    ]

    var isMonospace: Bool {
        Self.monospace.contains(self)
    }

    /// Applies the monospace digit style if allowed, otherwise returns none
    var monospace: Style {
        guard isMonospace else { return .none }
        return .monospace
    }
}
