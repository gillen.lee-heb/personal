import SwiftUI
import UIKit

public extension Typography.Component {
    /// Used to print the components name
    var demoString: String {
        switch self {
        case .title1:
            return "title1"
        case .title2:
            return "title2"
        case .title3:
            return "title3"
        case .title4:
            return "title4"
        case .title5:
            return "title5"
        case .title6:
            return "title6"
        case .body1:
            return "body1"
        case .caption1:
            return "caption1"
        case .subcaption1:
            return "subcaption1"
        case .footnote1:
            return "footnote1"
        case .body2:
            return "body2"
        case .caption2:
            return "caption2"
        case .subcaption2:
            return "subcaption2"
        case .footnote2:
            return "footnote2"
        }
    }
}

public extension Typography.Weight {
    /// Used to print the typography weight name
    var demoString: String {
        switch self {
        case .xxl900:
            return "xxl900"
        case .xl900:
            return "xl900"
        case .large900:
            return "large900"
        case .base400:
            return "base400"
        case .medium400:
            return "medium400"
        case .small400:
            return "small400"
        case .xs400:
            return "xs400"
        case .xxl400:
            return "xxl400"
        case .xl400:
            return "xl400"
        case .large400:
            return "large400"
        case .base800:
            return "base800"
        case .medium800:
            return "medium800"
        case .small800:
            return "small800"
        case .xs800:
            return "xs800"
        }
    }
}


public class DemoController: UIViewController {
    let vStack = UIStackView()

    public init() {
        vStack.axis = .vertical
        vStack.spacing = 30
        vStack.distribution = .equalCentering
        super.init(nibName: nil, bundle: nil)
    }

    public func adding(label: UILabel, text: AnyView) -> DemoController {
        label.layer.borderWidth = 1
        label.layer.borderColor = UIColor.black.cgColor
        label.backgroundColor = .lightGray

        let textHostingController = UIHostingController(rootView: text)
        textHostingController.view.bounds = label.bounds
        textHostingController.view.layer.borderWidth = 1
        textHostingController.view.layer.borderColor = UIColor.blue.cgColor
        textHostingController.view.backgroundColor = .lightGray

        let hStack = UIStackView(arrangedSubviews: [label, textHostingController.view])
        hStack.spacing = 20
        hStack.frame = CGRect(x: view.frame.origin.x, y: view.frame.origin.y, width: view.frame.size.height, height: label.frame.size.height)
        hStack.translatesAutoresizingMaskIntoConstraints = false

        vStack.addSubview(hStack)
        addChild(textHostingController)
        textHostingController.didMove(toParent: self)

        return self
    }

    public override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(vStack)
        vStack.frame = view.frame
    }

    required init?(coder: NSCoder) { nil }
}
