import SwiftUI
import UIKit

public class WeightTextBuilder: TextBuilder {
    let weight: Typography.Weight
    var style: Typography.Weight.Style
    var fontDecorator: TextDecorating

    static func xxl900() -> WeightTextBuilder {
        WeightTextBuilder(weight: .xxl900)
    }

    /// Initializes a weight text builder with the provided typography weight
    public init(weight: Typography.Weight) {
        self.weight = weight
        style = Typography.Weight.Style.none
        fontDecorator = FontDecorator(ctFont: weight.font)
        super.init()
        _ = paragraph(minimumLineHeight: weight.minimumLineHeight,
                  lineSpacing: weight.lineSpacing)
        _ = tracking(weight.tracking)
    }

    /// The default font weight is base400
    public override convenience init() {
        self.init(weight: Typography.Weight.base400)
    }

    /// Collection of all the decorators
    public override var allDecorators: [TextDecorating] {
        get {
            return decorators + [fontDecorator]
        }
    }

    /// Sets the default font for text in this text builder.
    public override func font(_ ctFont: CTFont) -> TextBuilder {
        fontDecorator = FontDecorator(ctFont: ctFont)
        style = Typography.Weight.Style.none
        return self
    }

    /// Sets the strikethrough stile for this text builder.
    public override func strikethrough() -> TextBuilder {
        guard validate(Typography.Weight.Style.strikethrough) else { return self }
        style = Typography.Weight.Style.strikethrough
        return super.strikethrough()
    }

    /// Sets the underline style for this text builder
    public override func underline() -> TextBuilder {
        guard validate(Typography.Weight.Style.underline) else { return self }
        style = Typography.Weight.Style.underline
        return super.underline()
    }

    /// Sets the monospace style for this text builder
    public override func monospace() -> TextBuilder {
        guard validate(Typography.Weight.Style.monospace) else { return self }
        style = Typography.Weight.Style.monospace
        fontDecorator = MonospaceDecorator(ctFont: weight.font)
        return self
    }

    /// Sets the italic style for this text builder
    public override func italic() -> TextBuilder {
        guard validate(Typography.Weight.Style.italic) else { return self }
        style = Typography.Weight.Style.italic
        fontDecorator = ItalicDecorator(ctFont: weight.font)
        return self
    }
}

extension WeightTextBuilder {
    /// Creates a new decorator with the provided style if it is valid for the current typography weight.
    func validate(_ newStyle: Typography.Weight.Style) -> Bool {
        // We only allow one style to be applied in Typography.
        // If our current style is not none, then we should return unchanged.
        guard case Typography.Weight.Style.none = style else { return false }

        // Check validation
        return weight.validate(newStyle) == newStyle
    }
}

extension UILabel {
    func xxl900() {
        updateWeight(.xxl900)
    }

    func updateWeight(_ weight: Typography.Weight) {
        let tb = WeightTextBuilder(weight: weight)
        tb.updateLabe(self)
    }
}

extension Text {
    func updateWeight(_ weight: Typography.Weight) -> AnyView {
        let tb = WeightTextBuilder(weight: weight)
        return tb.updateText(self)
    }

    func xxl900() -> AnyView {
        updateWeight(.xxl900)
    }
}
