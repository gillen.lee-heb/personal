import UIKit
import SwiftUI

//extension Typography.Component {
//    /// Decorates the given text with the typography component provided
//    struct Decorator: TextDecorating {
//        let component: Typography.Component
//        let weightDecorator: Typography.Weight.Decorator
//
//        func decorate(attributes: inout Attributes) {
//            weightDecorator
//                .decorate(attributes: &attributes)
//        }
//
//        func decorate(label: UILabel) {
//            weightDecorator
//                .decorate(label: label)
//        }
//
//        func decorate(text: Text) -> Text {
//            weightDecorator
//                .decorate(text: text)
//        }
//
//        func decorate(anyView: AnyView) -> AnyView {
//            AnyView(weightDecorator
//                        .decorate(anyView: anyView))
//        }
//    }
//}
//
//extension Typography.Component.Decorator {
//    /// Convenience initializer to create a new decorator with the given string and typography component
//    init(_ component: Typography.Component) {
//        self.init(component: component,
//                  weightDecorator: Typography.Weight.Decorator(component.weight))
//    }
//
//    /// Applies the italic style to the decorator
//    func italic() -> Typography.Component.Decorator {
//        style(with: Typography.Weight.Style.italic)
//    }
//
//    /// Applies the underline style to the decorator
//    func underline() -> Typography.Component.Decorator {
//        style(with: Typography.Weight.Style.underline)
//    }
//
//    /// Applies the strikethrough style to the decorator
//    func strikethrough() -> Typography.Component.Decorator {
//        style(with: Typography.Weight.Style.strikethrough)
//    }
//
//    /// Applies the monospace digit style to the decorator
//    func monospace() -> Typography.Component.Decorator {
//        style(with: Typography.Weight.Style.monospace)
//    }
//}
//
//extension Typography.Component.Decorator {
//    func style(with style: Typography.Weight.Style) -> Typography.Component.Decorator {
//        Typography.Component.Decorator(component: component,
//                                       weightDecorator: weightDecorator.style(with: style))
//    }
//}
//
//// MARK: TextBuilder Convenience
//
//extension TextBuilder {
//    func typographyComponent(component: Typography.Component) -> TextBuilder {
//        return decorate(with: Typography.Component.Decorator(component))
//    }
//}
