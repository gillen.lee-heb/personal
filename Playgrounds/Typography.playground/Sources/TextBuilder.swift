import SwiftUI
import UIKit

// MARK: NSAttributedString attributes alias

/// Convenience alias for attributes
public typealias Attributes = [NSAttributedString.Key : Any]

// MARK: TextDecorating protocol

/// Protocol for types that are used adorn or decorate text based views
public protocol TextDecorating {
    // MARK: UIKit

    /// Decorates NSAttributedString's attributes
    func decorate(attributes: inout Attributes)
    /// Decorates UILabel
    func decorate(label: UILabel)

    // MARK: SwiftUI

    /// Decorates Text
    func decorate(text: Text) -> Text
    /// Decorates AnyView
    func decorate(anyView: AnyView) -> AnyView
}

// MARK: TextBuilder class

/// Builds displayable text by providing decorators.
open class TextBuilder {
    private(set) var decorators = [TextDecorating]()
    var font: CTFont = UIFont.systemFont(ofSize: 16.0)

    public init() {}

    /// Adds a decorator to the text builder
    func decorate(with decorator: TextDecorating) -> TextBuilder {
        decorators.append(decorator)
        return self
    }

    open var allDecorators: [TextDecorating] {
        get {
            return decorators
        }
    }

    /// Builds a UILabel
    open func buildLabel(updatingExisting label: UILabel = UILabel()) -> UILabel {
        allDecorators.forEach { $0.decorate(label: label) }
        var attributes = Attributes()
        allDecorators.forEach { $0.decorate(attributes: &attributes) }
        label.attributedText = NSAttributedString(string: label.text ?? "", attributes: attributes)
        return label
    }

    /// Builds SwiftUI.Text wrapped in a AnyView
    open func buildText() -> AnyView {
        var text = Text("")
        allDecorators.forEach { text = $0.decorate(text: text) }
        var anyView = AnyView(text)
        allDecorators.forEach { anyView = $0.decorate(anyView: anyView) }
        return anyView
    }

    /// Sets the string for the text builder
    public func string(_ string: String) -> TextBuilder {
        return decorate(with: StringDecorator(string: string))
    }

    /// Sets the font for the text builder
    open func font(_ ctFont: CTFont) -> TextBuilder {
        font = ctFont
        return decorate(with: FontDecorator(ctFont: ctFont))
    }

    // Sets the paragraph settings for the text builder
    open func paragraph(minimumLineHeight: CGFloat,
                        lineSpacing: CGFloat) -> TextBuilder {
        return decorate(with: ParagraphDecorator(minimumLineHeight: minimumLineHeight, lineSpacing: lineSpacing))
    }

    /// Sets the tracking (aka kerning) for the text builder
    open func tracking(_ tracking: CGFloat) -> TextBuilder {
        return decorate(with: TrackingDecorator(tracking: tracking))
    }

    /// Sets that the text builder will be strikethrough
    open func strikethrough() -> TextBuilder {
        return decorate(with: StrikethroughDecorator())
    }

    /// Sets that the text builder will be underlined
    open func underline() -> TextBuilder {
        return decorate(with: UnderlineDecorator())
    }

    open func monospace() -> TextBuilder {
        return decorate(with: MonospaceDecorator(ctFont: font))
    }

    open func monospace(_ ctFont: CTFont) -> TextBuilder {
        font = ctFont
        return decorate(with: MonospaceDecorator(ctFont: ctFont))
    }

    open func italic() -> TextBuilder {
        return decorate(with: ItalicDecorator(ctFont: font))
    }

    open func italic(_ ctFont: CTFont) -> TextBuilder {
        font = ctFont
        return decorate(with: ItalicDecorator(ctFont: ctFont))
    }
}

// MARK: - TEXT DECORATORS

// MARK: String Decorator

/// Decorates the text builder with a provided plain-text string
struct StringDecorator: TextDecorating {
    let string: String

    init(string: String) { self.string = string }

    func decorate(attributes: inout Attributes) {}

    func decorate(label: UILabel) { label.text = string }

    func decorate(text: Text) -> Text { text + Text(string) }

    func decorate(anyView: AnyView) -> AnyView { anyView }
}

// MARK: Font Decorator

/// Decorates the text builder by providing font details such as font size and weight
struct FontDecorator: TextDecorating {
    let ctFont: CTFont

    init(ctFont: CTFont) { self.ctFont = ctFont }

    func decorate(attributes: inout Attributes) { attributes[.font] = ctFont as UIFont }

    func decorate(label: UILabel) {}

    func decorate(text: Text) -> Text {
        let newText = text.font(Font(ctFont))
        return newText
    }

    func decorate(anyView: AnyView) -> AnyView { anyView }
}

// MARK: Paragraph Decorator

/// Decorates paragraph content like line spacing
struct ParagraphDecorator: TextDecorating {
    let minimumLineHeight: CGFloat
    let lineSpacing: CGFloat

    init(minimumLineHeight: CGFloat, lineSpacing: CGFloat) {
        self.minimumLineHeight = minimumLineHeight
        self.lineSpacing = lineSpacing
    }

    func decorate(attributes: inout Attributes) {
        let paragraph = NSMutableParagraphStyle()
        paragraph.minimumLineHeight = minimumLineHeight
        paragraph.lineSpacing = lineSpacing

        attributes[.paragraphStyle] = paragraph
    }

    func decorate(label: UILabel) {}

    func decorate(text: Text) -> Text { text }

    func decorate(anyView: AnyView) -> AnyView { AnyView(anyView.lineSpacing(lineSpacing)) }
}

// MARK: Tracking Decorator

/// Decorates with a tracking (aka character spacing) value
struct TrackingDecorator: TextDecorating {
    let tracking: CGFloat

    init(tracking: CGFloat) { self.tracking = tracking }

    func decorate(attributes: inout Attributes) {
        if #available(iOS 14, *) {
            attributes[.tracking] = tracking
        } else {
            attributes[.kern] = tracking
        }
    }

    func decorate(label: UILabel) {}

    func decorate(text: Text) -> Text { text.tracking(tracking) }

    func decorate(anyView: AnyView) -> AnyView { anyView }
}

// MARK: Strikethrough Decorator

/// Decorates by applying strikethrough to the text
struct StrikethroughDecorator: TextDecorating {
    init() {}

    func decorate(attributes: inout Attributes) { attributes[.strikethroughStyle] = NSUnderlineStyle.single.rawValue }

    func decorate(label: UILabel) {}

    func decorate(text: Text) -> Text { text.strikethrough() }

    func decorate(anyView: AnyView) -> AnyView { anyView }
}

// MARK: Underline Decorator

/// Decorates by applying underline to the text
struct UnderlineDecorator: TextDecorating {
    init() {}

    func decorate(attributes: inout Attributes) { attributes[.underlineStyle] = NSUnderlineStyle.single.rawValue }

    func decorate(label: UILabel) {}

    func decorate(text: Text) -> Text { text.underline() }

    func decorate(anyView: AnyView) -> AnyView { anyView }
}

// MARK: Monospace Decorator

/// Decorates by applying monospace digits to the given font
struct MonospaceDecorator: TextDecorating {
    let ctFont: CTFont

    init(ctFont: CTFont) { self.ctFont = ctFont }

    func decorate(attributes: inout Attributes) {
        let fontDescriptorFeatureSettings = [[
            UIFontDescriptor.FeatureKey.featureIdentifier: kNumberSpacingType,
            UIFontDescriptor.FeatureKey.typeIdentifier: kMonospacedNumbersSelector
        ]]
        let fontDescriptorAttributes = [
            UIFontDescriptor.AttributeName.featureSettings: fontDescriptorFeatureSettings
        ]

        let descriptor = (ctFont as UIFont).fontDescriptor.addingAttributes(fontDescriptorAttributes)

        //size 0 means keep the size as it is
        attributes[.font] = UIFont(descriptor: descriptor, size: 0)
    }

    func decorate(label: UILabel) {}

    func decorate(text: Text) -> Text { text.font(Font(ctFont).monospacedDigit()) }

    func decorate(anyView: AnyView) -> AnyView { anyView }
}

// MARK: Italic Decorator

/// Decorates by applying italic to the given font
struct ItalicDecorator: TextDecorating {
    let ctFont: CTFont

    init(ctFont: CTFont) { self.ctFont = ctFont }

    func decorate(attributes: inout Attributes) {
        var font = ctFont as UIFont
        defer { attributes[.font] = font }
        
        let descriptor = (ctFont as UIFont).fontDescriptor
        let traits = descriptor.symbolicTraits.union(.traitItalic)
        guard let italicsDescriptor = descriptor.withSymbolicTraits(traits) else {
            // failed to apply italics
            attributes[.font] = ctFont as UIFont
            return
        }

        //size 0 means keep the size as it is
        font = UIFont(descriptor: italicsDescriptor, size: 0)
    }

    func decorate(label: UILabel) {}

    func decorate(text: Text) -> Text { text.font(Font(ctFont).italic()) }

    func decorate(anyView: AnyView) -> AnyView { anyView }
}
